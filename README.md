`pipeleak` is inspired by pipeview, `pv(1)`, for monitoring progress in a text pipeline.
It copies stdin to stdout,  sampling lines to stderr.
Lines are numbered, with an optional elapsed time.
Pipe data passes through with minimal buffering,
so it can be inserted into a process pipeline.

Sampling emphasizes the beginning and end of the pipe, with 
fewer samples from the middle.
Sampling is roughly logarithmic in the number of lines,
so it handles large pipes without too much output.
Sample memory is also roughly logarithmic,
to provide more detail at the end.

Samples include the first 5 lines,
followed by every 10th line up to line 50,
followed by every 100th line up to line 500, and so on. 
At the end of the file, samples include the last few lines,
after the last few multiples of 10, after the last few multiples of 100, etc.

Note that sampling at the end-of-file does not generally mirror sampling at the beginning, because the end isn't known in advance.
However, the last line is always included,
and serves as a line count for the file.

Things misbehave if the line count exceeds `LONG_MAX`.

## USAGE

Options are:

    -q      suppress copy to stdout, samples only
    -n      suppress line numbers
    -t      prefix with elapsed time


## EXAMPLES

    seq 3333 | ./pipeleak -nq
    
    zcat < rdns.gz | ./pipeleak -q

    zcat rdns.gz | ./pipeleak | fgrep -c .he.net.

