#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <math.h>
#include <argp.h>

#define RADIX  10
#define BURST  5

// output flags
static unsigned char optts = 0, optln = 1, optout = 1;

// latest emit
static long emitn = 0;
// emit a record
static void
emit(long ln, float ts, const char *line) {
	if (optts) fprintf(stderr, "%+.3f\t", ts);
	if (optln) fprintf(stderr, "%6ld\t", ln);
	fputs(line, stderr);
	emitn = ln;
}

// for each power, we hold the last few lines of that power
// so we can give trailing samples
struct held {
	long ln;
	float ts;
	char *line;
};

// enough for 2^64
#define POWLIMIT 20
static int powlim = POWLIMIT;

// burst of holds for each power
static int holdc = 0;
static struct held holds[POWLIMIT][BURST];

// hold a line in case we're near the end
static void
hold(int p, int r, long ln, float ts, const char *line) {
	struct held *h;
	if (p == holdc && holdc + 1 < powlim) holdc++;
	// update hold
	h = &holds[p][r % BURST];
	if (h->line) free(h->line);
	h->line = strdup(line);
	h->ln = ln;
	h->ts = ts;
}

// holds get sorted into line number order
static int
draincmp(const void *v0, const void *v1) {
	const struct held *h0 = v0, *h1 = v1;
	// int-size result from long compares
	return h0->ln < h1->ln ? -1 : h1->ln < h0->ln ? +1 : 0;
}

// drain the held samples in line number order
static void
drain(void) {
	for (int p = holdc - 1; 0 <= p; --p) {
	    // sort the holds back into order
		qsort(holds[p], BURST, sizeof *holds[p], draincmp);
		for (int r = 0; r < BURST; r++) {
			struct held *h = &holds[p][r];
			if (emitn < h->ln) emit(h->ln, h->ts, h->line);
		}
	}
}

// collect a timestamp
static double
sampts(void) {
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return (tv.tv_sec + 1e-6 * tv.tv_usec);
}

static long sampln = 0;
static double sampts0 = 0.0;

static void
sample(const char *line) {
	int n = ++sampln, p = 0, r;
	double ts = sampts0;
	// timestamps iff enabled
	if (optts) ts = sampts() - ts;
	// factor (ln) into (RADIX ** p * _ + r) where (p) is maximized and (0 < r < RADIX)
	// so r is leading digit, p counts trailing 0s
	for (;;) {
		div_t d = div(n, RADIX);
		r = d.rem;
		if (r != 0) break;
		n = d.quot;
		if (p + 1 == powlim) break;
		p += 1;
	}
	if (p < holdc || BURST < r) {
		// hold for later
		hold(p, r, sampln, ts, line);
	}
	else {
		// fresh sample
		emit(sampln, ts, line);
	}
}

// argp config

// options
static struct argp_option options[] = {
	{NULL, 'q', 0, 0, "don't copy to stdout"},
	{NULL, 't', 0, 0, "include elapsed time"},
	{NULL, 'n', 0, 0, "don't include line numbers"},
	{NULL, 'N', "COUNT", 0, "limit line intervals"},
	{0}
};

// argp option handler
static error_t
parse(int key, char *arg, struct argp_state *state)
{	char *err = NULL;
	switch (key) {
	case 't':
		optts = 1;
		sampts0 = sampts();
		break;
	case 'q':
		optout = 0;
		break;
	case 'n':
		optln = 0;
		break;
	case 'N':
		powlim = strtol(arg, &err, 10);
		if (*err) {
			argp_usage(state);
		}
		else if (POWLIMIT < powlim) {
			fprintf(stderr, "-N %d too large\n", powlim);
			powlim = POWLIMIT;
		}
		break;
	case ARGP_KEY_ARG:
		// no other args
		argp_usage(state);
		break;
	case ARGP_KEY_INIT:
	case ARGP_KEY_END:
		break;
	default:
		return ARGP_ERR_UNKNOWN;
	}
	return 0;
}

// argp config
static struct argp argp = { options, parse, NULL, "leak samples of piped data to stderr" };

int
main(int argc, char *argv[]) {
	char *line = NULL;
	size_t linesize = 0;
	argp_parse(&argp, argc, argv, 0, 0, NULL);
	for (;;) {
		int len = getline(&line, &linesize, stdin);
		if (len < 0) break;
		if (optout) fputs(line, stdout);
		sample(line);
	}
	drain();
}
